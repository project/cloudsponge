<!-- Any link with a class="cs_import" will start the import process -->
<p><?php print t("Select people from your address book:") ?></p>
<span>
  <?php foreach ($providers as $provider => $link): ?>
    <a class="cloudsponge-launch" data-cloudsponge-source="<?php print $provider ?>">
        <img src="<?php print $link ?>" alt="<?php print $provider?>" />
    </a>
  <?php endforeach ?>
</span>
<p>
  <?php print render($cloudsponge_contacts_textarea); ?>
</p>
