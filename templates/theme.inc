<?php

/**
 * Preprocessor for theme('cloudsponge_widget').
 */
function cloudsponge_preprocess_cloudsponge_widget(&$vars) {
  cloudsponge_add_js();

  $providers = array_filter(variable_get('cloudsponge_providers', array()));

  $links = array(
    'linkedin'    => CS_PROVIDER_IMG_PATH . 'linkedin.png',
    'yahoo'       => CS_PROVIDER_IMG_PATH . 'yahoo.png',
    'gmail'       => CS_PROVIDER_IMG_PATH . 'gmail.png',
    'aol'         => CS_PROVIDER_IMG_PATH . 'aol.png',
    'plaxo'       => CS_PROVIDER_IMG_PATH . 'plaxo.png',
    'addressbook' => CS_PROVIDER_IMG_PATH . 'macoscontacts.png',
    'outlook'     => CS_PROVIDER_IMG_PATH . 'outlook.png',
  );

  $vars['providers'] = array();
  foreach ($providers as $provider) {
    $vars['providers'][$provider] = $links[$provider];
  }

  $settings = array(
    'redirect' => (bool)variable_get('cloudsponge_redirect', FALSE),
    'redirect_url' => url(token_replace(variable_get('cloudsponge_redirect_url', ''))),
    'skip_choosing' => (bool)variable_get('cloudsponge_skip_choosing', FALSE),
  );

  drupal_add_js(array('cloudsponge' => $settings), 'setting');
}
