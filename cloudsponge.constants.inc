<?php
/**
 * @file
 *  <p>Constants for CloundSponge module.</p>
 */

/**
 * The location for CloudSponge provider's image directory.
 *
 * @var string
 */
define('CS_PROVIDER_IMG_PATH', '//www.cloudsponge.com/img/developer/address_books/flat/');
